package ru.andreymarkelov.atlas.plugins;

import java.util.HashMap;
import java.util.Map;

import ru.andreymarkelov.atlas.plugins.amutils.util.Utils;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

public class EstimatePostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {
    private final CustomFieldManager customFieldManager;

    public EstimatePostFunctionFactory(CustomFieldManager customFieldManager) {
        this.customFieldManager = customFieldManager;
    }

    private String getCustomFieldName(String customFieldId) {
        if (customFieldId == null) {
            return "";
        }

        CustomField customField = customFieldManager.getCustomFieldObject(Long.parseLong(customFieldId));
        if (customField != null) {
            return customField.getName();
        }
        return customFieldId;
    }

    private Map<Long, String> getCustomFields() {
        Map<Long, String> customFields = new HashMap<>();
        for (CustomField customField : customFieldManager.getCustomFieldObjects()) {
            customFields.put(customField.getIdAsLong(), customField.getName());
        }
        return Utils.sortByValues(customFields);
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> functionParams) {
        Map<String, Object> map = new HashMap<String, Object>();

        if (functionParams != null && functionParams.containsKey("customFieldId")) {
            map.put("customFieldId", extractSingleParam(functionParams, "customFieldId"));
        } else {
            map.put("customFieldId", "");
        }

        if (functionParams != null && functionParams.containsKey("inhours")) {
            map.put("inhours", extractSingleParam(functionParams, "inhours"));
        } else {
            map.put("inhours", "");
        }

        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor validatorDescriptor = (FunctionDescriptor) descriptor;
        String value = (String) validatorDescriptor.getArgs().get(param);
        if (value!=null && value.trim().length() > 0) {
            return value;
        }
        return "";
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("customFields", getCustomFields());
        velocityParams.put("customFieldId", getParam(descriptor, "customFieldId"));
        velocityParams.put("inhours", getParam(descriptor, "inhours"));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put("customFields", getCustomFields());
        velocityParams.put("customFieldId", "");
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("customFieldId", getCustomFieldName(getParam(descriptor, "customFieldId")));
        velocityParams.put("inhours", getParam(descriptor, "inhours"));
    }
}
