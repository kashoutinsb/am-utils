package ru.andreymarkelov.atlas.plugins.amutils.manager;

import java.util.List;

public interface GlobalSettingsManager {
    List<Long> getAttachmentListenerProjectIds();
    void setAttachmentListenerProjectsIds(List<Long> projectsIds);
}
